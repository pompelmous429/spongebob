//
//  Boss.swift
//  SpongeBob
//
//  Created by mac19 on 2021/6/15.
//  Copyright © 2021 mac19. All rights reserved.
//

import Foundation
import SpriteKit

class Boss: SKSpriteNode {
    var bTexture: SKTexture
    var direction: String
    var bossRAction: SKAction
    var bossLAction: SKAction
    
    init(direction: String) {
        
        self.direction = direction
        
        if direction == "left" {
            bTexture = SKTexture(imageNamed: "boss_left_64_01.png")
        }
        else {// direction == "right"
            bTexture = SKTexture(imageNamed: "boss_right_64_01")
        }
        
        
        var FLtextures:[SKTexture] = []
        for i in 1...3 {
            FLtextures.append(SKTexture(imageNamed: "boss_left_64_0\(i).png"))
        }
        FLtextures.append(SKTexture(imageNamed: "boss_left_64_02.png"))
        FLtextures.append(SKTexture(imageNamed: "boss_left_64_01.png"))
        bossLAction = SKAction.animate(with: FLtextures, timePerFrame: 0.08)
    
        
        var FRtextures:[SKTexture] = []
        for i in 1...3 {
            FRtextures.append(SKTexture(imageNamed: "boss_right_64_0\(i).png"))
        }
        FRtextures.append(SKTexture(imageNamed: "boss_right_64_02.png"))
        FRtextures.append(SKTexture(imageNamed: "boss_right_64_01.png"))
        bossRAction = SKAction.animate(with: FRtextures, timePerFrame: 0.08)
        
        
        super.init(texture: bTexture, color: UIColor.clear, size: bTexture.size())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
