//
//  MyCharacter.swift
//  SpongeBob
//
//  Created by user198529 on 5/29/21.
//  Copyright © 2021 mac19. All rights reserved.
//

import UIKit
import SpriteKit


class MyCharacter: SKSpriteNode {
    
    var characterName: String
    var health: Int
    var bombNumber: Int
    var velocity: CGFloat
    var firePower: Int
    
    var unbeatable: Bool
    var characterUnbeatableAction: SKAction!
    
    var characterRAction: SKAction!
    var characterLAction: SKAction!
    var direction: String!// right or left, used to decide which texture should be
    
    var cTexture: SKTexture!
    
    
    init(cName: String) {
        self.characterName = cName
        self.direction = "left"
        self.health = 4
        self.bombNumber = 2
        self.velocity = 0.025
        self.firePower = 1
        self.unbeatable = false
        
        self.cTexture = SKTexture(imageNamed: cName + "_front_left_64_01.png") // default is left direction
        
        var FLtextures:[SKTexture] = []
        for i in 1...5 {
            FLtextures.append(SKTexture(imageNamed: cName + "_front_left_64_0\(i).png"))
        }
        FLtextures.append(SKTexture(imageNamed: cName + "_front_left_64_04.png"))
        FLtextures.append(SKTexture(imageNamed: cName + "_front_left_64_03.png"))
        FLtextures.append(SKTexture(imageNamed: cName + "_front_left_64_02.png"))
        FLtextures.append(SKTexture(imageNamed: cName + "_front_left_64_01.png"))
        characterLAction = SKAction.animate(with: FLtextures, timePerFrame: 0.04)
        //characterLAction = SKAction.group([SKAction.animate(with: FLtextures, timePerFrame: 0.04)])
        
        characterUnbeatableAction = SKAction.sequence([SKAction.colorize(with: UIColor.clear, colorBlendFactor: 1.0, duration: 0.1), SKAction.colorize(with: UIColor.white, colorBlendFactor: 1.0, duration: 0.1)])
        
        
        
        var FRtextures:[SKTexture] = []
        for i in 1...5 {
            FRtextures.append(SKTexture(imageNamed: cName + "_front_right_64_0\(i).png"))
        }
        FRtextures.append(SKTexture(imageNamed: cName + "_front_right_64_04.png"))
        FRtextures.append(SKTexture(imageNamed: cName + "_front_right_64_03.png"))
        FRtextures.append(SKTexture(imageNamed: cName + "_front_right_64_02.png"))
        FRtextures.append(SKTexture(imageNamed: cName + "_front_right_64_01.png"))
        characterRAction = SKAction.animate(with: FRtextures, timePerFrame: 0.04)
        
        super.init(texture: cTexture, color: UIColor.clear, size: cTexture.size())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
