//
//  Bomb.swift
//  SpongeBob
//
//  Created by mac19 on 2021/6/11.
//  Copyright © 2021 mac19. All rights reserved.
//


import Foundation
import SpriteKit

class Bomb: SKSpriteNode {
    var bPosition: CGPoint!
    var bTexture: SKTexture!
    var bombAction: SKAction!
    
    init(bPos: CGPoint) {
        bPosition = bPos
        bTexture = SKTexture(imageNamed: "bomb_01")
        
        var textures: [SKTexture] = []
        for i in 1...4 {
            textures.append(SKTexture(imageNamed: "bomb_0\(i)"))
        }
        textures.append(SKTexture(imageNamed: "bomb_03"))
        textures.append(SKTexture(imageNamed: "bomb_02"))
        textures.append(SKTexture(imageNamed: "bomb_01"))
        bombAction = SKAction.group([SKAction.repeatForever(SKAction.animate(with: textures, timePerFrame: 0.1)), SKAction.sequence([SKAction.wait(forDuration: 5), SKAction.removeFromParent()])])
        //, SKAction.removeFromParent()
        
        super.init(texture: bTexture, color: UIColor.clear, size: bTexture.size())
        //self.parent?.addChild(Bomb(bPos: CGPoint(x: self.position.x + 100, y: self.position.y)))
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
