//
//  TileNode.swift
//  SpongeBob
//
//  Created by mac19 on 2021/6/12.
//  Copyright © 2021 mac19. All rights reserved.
//

import Foundation
import SpriteKit

class TileNode: SKSpriteNode {
    var stability: Int
    
    init(texture: SKTexture, stability: Int) {
        self.stability = stability
        
        super.init(texture: texture, color: UIColor.clear, size: texture.size())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
