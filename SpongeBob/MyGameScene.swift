//
//  MyGameScene.swift
//  SpongeBob
//
//  Created by user198529 on 5/28/21.
//  Copyright © 2021 mac19. All rights reserved.
//


// ------- bugs -------
// 1. when two players have same characterName, bomb naming will overlap
// 2. in fire-bomb contact, should I pass which bomb to remove to others?
// 3. in createFire(), should I check which firePower to use?
// 4. sometimes app crashes when running on simulator
//    -- maybe some error when passing data to other peers
//    -- maybe because dict["removeChild"] already not exist anymore?


import Foundation
import UIKit
import SpriteKit

import MultipeerConnectivity


class ColliderType {
    static let playerCollider: UInt32 = 0x1 << 0
    static let monsterCollider: UInt32 = 0x1 << 1
    static let tileCollider: UInt32 = 0x1 << 2
    static let bombCollider: UInt32 = 0x1 << 3
    static let fireCollider: UInt32 = 0x1 << 4
    static let itemCollider: UInt32 = 0x1 << 5
    static let wallCollider: UInt32 = 0x1 << 6
}

class MyGameScene: SKScene, SKPhysicsContactDelegate {
    let cameraNode: SKCameraNode!
    var peerID: MCPeerID!
    var mcSession: MCSession!
    var nearbyServiceAdvertiser: MCNearbyServiceAdvertiser!
    
    
    var mapNode: SKTileMapNode!
    var bgm: SKAction!
    var hurt_sound: SKAction!
    var explosion_sound: SKAction!
    var item_get_sound: SKAction!
    
    var myCharacter : MyCharacter!
    var enemy: MyCharacter = MyCharacter(cName: "squid")
    var myHealth: SKLabelNode!
    var enemyHealth: SKLabelNode!
    
    
    // bosses
    var boss_01: Boss!
    var boss_02: Boss!
    var boss_03: Boss!
    

    
    var bombClicker: SpriteKitButton!
    lazy var analogJoystick: AnalogJoystick = {
        let js = AnalogJoystick(diameter: 100, colors: nil, images: (substrate: #imageLiteral(resourceName: "jSubstrate"), stick: #imageLiteral(resourceName: "jStick")))
        js.position = CGPoint(x: self.anchorPoint.x - 300, y: self.anchorPoint.y - 400)
      js.zPosition = 2
      return js
    }()
    
    
    
    override init() {
        
        print("GameScene init() is called!")
        cameraNode = SKCameraNode()
        super.init()
        // init() not called, because we are initializing GameScene with .sks file
    }
    
    required init?(coder aDecoder: NSCoder) {
        cameraNode = SKCameraNode()
        
        super.init(coder: aDecoder)
    }
    
    
    override func didMove(to view: SKView) {
        self.myCharacter = MyCharacter(cName: self.userData!["cName"] as! String)
        getSceneNodes()
        setup()
        showConnectionPrompt()
        handleTileMapNode()
        
//        print("GameScene userData : ")
//        for (key, value) in self.userData! {
//            print("key \(key), value: \(value)")
//        }

    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        cameraNode.position = myCharacter.position
        self.camera?.position = cameraNode.position
        
        // joystick and bombClicker position
        self.bombClicker.position = CGPoint(x: self.myCharacter.position.x + 300, y: self.myCharacter.position.y - 400)
        analogJoystick.position = CGPoint(x: myCharacter.position.x - 300, y: myCharacter.position.y - 400)
        
        // myCharacter and enemy texture
        self.enemy.texture = SKTexture(imageNamed: (self.enemy.characterName + "_front_" + self.enemy.direction + "_64_01.png"))
        self.myCharacter.texture = SKTexture(imageNamed: (self.myCharacter.characterName + "_front_" + self.myCharacter.direction + "_64_01.png"))
        
        
        
        // myCharacter and enemy health present
        self.myHealth.text = "\(self.myCharacter.characterName): \(self.myCharacter.health)"
        self.myHealth.position = CGPoint(x: self.myCharacter.position.x - ScreenSize.width / 3, y: self.myCharacter.position.y + ScreenSize.height / 3)
        
        self.enemyHealth.text = "\(self.enemy.characterName): \(self.enemy.health)"
        self.enemyHealth.position = CGPoint(x: self.myCharacter.position.x + ScreenSize.width / 3, y: self.myCharacter.position.y + ScreenSize.height / 3)
        
        
        if (self.myCharacter.unbeatable == true) {
            if (self.myCharacter.action(forKey: "UnbAction") == nil) {
                self.myCharacter.run(self.myCharacter.characterUnbeatableAction, withKey: "UnbAction")
            }
        }
        
        if (self.enemy.unbeatable == true) {
            if (self.enemy.action(forKey: "UnbAction") == nil) {
                self.enemy.run(self.enemy.characterUnbeatableAction, withKey: "UnbAction")
            }
        }

        // Ending condition check
        if (self.myCharacter.health == 0 || self.enemy.health == 0) {
            let gameOverScene = GameOverScene(size: (self.view?.bounds.size)!)
            
            // pass character data to game scene
            gameOverScene.userData = NSMutableDictionary()
            if self.myCharacter.health == 0 {
                gameOverScene.userData?.setValue("\(self.enemy.characterName)", forKey: "winner")
            }
            else {
                gameOverScene.userData?.setValue("\(self.myCharacter.characterName)", forKey: "winner")
            }
            //gameOverScene.userData?.setValue(self.characterButton.currentTitle, forKey: "cName") // pass ("cName", "bob/patrick/squid") to game scene
            
            self.view?.presentScene(gameOverScene)
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
}

extension MyGameScene: MCSessionDelegate {
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .connected:
            print("Connected: \(peerID.displayName)")
            self.enemy.isHidden = false
            
            // tell others we are now in
            var dict = [String: String]()
            dict["cName"] = self.myCharacter.characterName
            
            let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
            try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
            
        case .connecting:
            print("Connecting: \(peerID.displayName)")
        case .notConnected:
            print("NotConnected: \(peerID.displayName)")
        default:
            fatalError()
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
            let dict = json as! Dictionary<String, String>
            for (key, value) in dict {
                
                switch key {
                case "cName":
                    
                    if(self.enemy.characterName != value) {
                        let enemyPosition = self.enemy.position
                        self.enemy.removeFromParent()
                        
                        self.enemy = MyCharacter(cName: value)
                        self.enemy.position = enemyPosition
                        self.enemy.zPosition = 5
                        self.enemy.name = "enemy"
                        self.enemy.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: self.enemy.size.width - 15, height: self.enemy.size.height - 5), center: self.enemy.anchorPoint)
                        self.enemy.physicsBody?.categoryBitMask = ColliderType.playerCollider
                        self.enemy.physicsBody?.collisionBitMask = ColliderType.tileCollider | ColliderType.wallCollider
                        self.enemy.physicsBody?.contactTestBitMask = ColliderType.monsterCollider | ColliderType.fireCollider | ColliderType.itemCollider
                        self.enemy.physicsBody?.allowsRotation = false
                        self.enemy.zRotation = 0
                        addChild(self.enemy)
                    }
                    self.enemyHealth.isHidden = false
                    self.enemy.isHidden = false
                    
                    
                case "moveX": // need to check left or right direction
                    if CGFloat(Double(value)!) > 0 {// moves toward +X
                        self.enemy.direction = "right"
                        if(self.enemy.action(forKey: "RAction") == nil) {
                            self.enemy.run(self.enemy.characterRAction, withKey: "RAction")
                        }
                    }
                    else {// moves toward -X
                        self.enemy.direction = "left"
                        if(self.enemy.action(forKey: "LAction") == nil) {
                            self.enemy.run(self.enemy.characterLAction, withKey: "LAction")
                        }
                    }
                    
                    self.enemy.position = CGPoint(x: self.enemy.position.x + CGFloat(Double(value)!) * self.enemy.velocity, y: self.enemy.position.y)
                    
                    
                case "moveY":
                    if self.enemy.direction == "left" {
                        if(self.enemy.action(forKey: "LAction") == nil) {
                            self.enemy.run(self.enemy.characterLAction, withKey: "LAction")
                        }
                    }
                    else {// enemy.direction == "right"
                        if(self.enemy.action(forKey: "RAction") == nil) {
                            self.enemy.run(self.enemy.characterRAction, withKey: "RAction")
                        }
                    }
                    
                    self.enemy.position = CGPoint(x: self.enemy.position.x, y: self.enemy.position.y + CGFloat(Double(value)!) * self.enemy.velocity)
                    
                case "bomb":// value is the bomb name
                    let bomb = Bomb(bPos: self.enemy.position)
                    bomb.position = self.enemy.position
                    bomb.zPosition = 4
                    bomb.name = value
                    bomb.physicsBody = SKPhysicsBody(texture: bomb.bTexture, size: bomb.bTexture.size())
                    bomb.physicsBody?.categoryBitMask = ColliderType.bombCollider
                    bomb.physicsBody?.collisionBitMask = 0
                    bomb.physicsBody?.contactTestBitMask = ColliderType.fireCollider
                    self.addChild(bomb)
                    bomb.run(bomb.bombAction)
                    
                case "fire":
                    let x = CGFloat(Double((value.components(separatedBy: ",")[0]))!)
                    let y = CGFloat(Double((value.components(separatedBy: ",")[1]))!)
                    let power = Int((value.components(separatedBy: ",")[2]))!
                    print("x: \(x), y: \(y)")
                    
                    let fire_x = Fire(firePower: power, direction: "x")
                    fire_x.position = CGPoint(x: x, y: y)
                    fire_x.zPosition = 4
                    fire_x.name = "fire"
                    fire_x.physicsBody = SKPhysicsBody(texture: fire_x.texture!, size: (fire_x.texture?.size())!)
                    fire_x.physicsBody?.categoryBitMask = ColliderType.fireCollider
                    fire_x.physicsBody?.collisionBitMask = 0
                    fire_x.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
                    addChild(fire_x)
                    fire_x.run(fire_x.fireAction)
                    
                    let fire_y = Fire(firePower: power, direction: "y")
                    fire_y.position = CGPoint(x: x, y: y)
                    fire_y.zPosition = 4
                    fire_y.name = "fire"
                    fire_y.physicsBody = SKPhysicsBody(texture: fire_y.texture!, size: (fire_y.texture?.size())!)
                    fire_y.physicsBody?.categoryBitMask = ColliderType.fireCollider
                    fire_y.physicsBody?.collisionBitMask = 0
                    fire_y.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
                    addChild(fire_y)
                    fire_y.run(fire_y.fireAction)
                    
                case "setUnb":
                    if value == "true" {// set enemy.unbeatable to true
                        self.enemy.unbeatable = true
                    }
                    else if value == "false" {
                        self.enemy.unbeatable = false
                    }
                    else {
                        print("Unexpected parameter in setUnb")
                    }
                    
                case "removeChild":
                    if let child = self.childNode(withName: value) as? SKSpriteNode {
                        print("Going to remove \(child.name!)")
                        child.removeFromParent()
                    }
                    
                //case "removeTile":
                    
                    
                case "subStability":
                    if let child = self.childNode(withName: value) as? TileNode {
                        child.stability -= 1
                    }
                    
                case "setVelocity":
                    self.enemy.velocity = CGFloat(Double(value)!)
                    
                case "setHealth":
                    self.enemy.health = Int(value)!
                    
                case "setFirePower":
                    self.enemy.firePower = Int(value)!
                    
                case "applyForce":
                    if let child = self.childNode(withName: value.components(separatedBy: ",")[0]) as? Boss {
                        let direction_x = Int(value.components(separatedBy: ",")[1])
                        let direction_y = Int(value.components(separatedBy: ",")[2])
                        let magnitude_x = CGFloat(Double(value.components(separatedBy: ",")[3])!)
                        let magnitude_y = CGFloat(Double(value.components(separatedBy: ",")[4])!)
                        
                        if direction_x == 0 {// +x
                            if (child.action(forKey: "RAction") == nil) {
                                child.removeAllActions()
                                child.run(SKAction.repeatForever(child.bossRAction), withKey: "RAction")
                            }
                            if direction_y == 0 {// +y
                                child.physicsBody?.applyForce(CGVector(dx: magnitude_x, dy: magnitude_y))
                            }
                            else {// -y
                                child.physicsBody?.applyForce(CGVector(dx: magnitude_x, dy: -magnitude_y))
                            }
                        }
                        else {// -x
                            if (child.action(forKey: "LAction") == nil) {
                                child.removeAllActions()
                                child.run(SKAction.repeatForever(child.bossRAction), withKey: "LAction")
                            }
                            if direction_y == 0 {// +y
                                child.physicsBody?.applyForce(CGVector(dx: -magnitude_x, dy: magnitude_y))
                            }
                            else {// -y
                                child.physicsBody?.applyForce(CGVector(dx: -magnitude_x, dy: -magnitude_y))
                            }
                        }
                        
                    }
                    
                default:
                    print("Switch default")
                }
            }
            
        } catch _ {
            print("Error in converting Data to Dictionary.")
        }
        
    }
    
    
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    
    
}

extension MyGameScene: MCBrowserViewControllerDelegate {
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        browserViewController.dismiss(animated: true, completion: nil)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        browserViewController.dismiss(animated: true, completion: nil)
        
    }
    
    
}

extension MyGameScene: MCNearbyServiceAdvertiserDelegate {
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        invitationHandler(true, mcSession)
    }
    
    
}


// my own functions
extension MyGameScene {
    
    func getSceneNodes() {// get links of items in .sks file
        
        for node in self.children {
            if (node.name?.contains("item")) == true {
                print("Find item: \((node.name)!)")
                
                if let itemNode: SKSpriteNode = self.childNode(withName: (node.name)!) as? SKSpriteNode {
                    
                    itemNode.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 50, height: 50))
                    itemNode.physicsBody?.categoryBitMask = ColliderType.itemCollider
                    itemNode.physicsBody?.collisionBitMask = 0
                    itemNode.physicsBody?.contactTestBitMask = ColliderType.playerCollider
                }
            }
            if (node.name?.contains("wall")) == true {
                print(node.name!)
                if let wallNode: SKSpriteNode = self.childNode(withName: (node.name)!) as? SKSpriteNode {
                    wallNode.physicsBody = SKPhysicsBody(texture: wallNode.texture!, size: (wallNode.texture?.size())!)
                    wallNode.physicsBody?.categoryBitMask = ColliderType.wallCollider
                    wallNode.physicsBody?.collisionBitMask = ColliderType.playerCollider
                    wallNode.physicsBody?.allowsRotation = false
                    wallNode.physicsBody?.isDynamic = false
                }
            }
            
            if (node.name?.contains("boss")) == true {
                if node.name == "boss_01" {
                    print("Find boss_01")
                    boss_01 = Boss(direction: "left")
                    boss_01.position = node.position
                    boss_01.name = "boss_01"
                    self.addChild(boss_01)
                    node.removeFromParent()
                    
                    boss_01.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: boss_01.size.width - 20, height: boss_01.size.height), center: boss_01.anchorPoint)
                    boss_01.physicsBody?.categoryBitMask = ColliderType.monsterCollider
                    boss_01.physicsBody?.collisionBitMask = ColliderType.tileCollider | ColliderType.wallCollider | ColliderType.bombCollider
                    boss_01.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.fireCollider
                    boss_01.physicsBody?.isDynamic = true
                    boss_01.physicsBody?.allowsRotation = false
                }
                if node.name == "boss_02" {
                    boss_02 = Boss(direction: "left")
                    boss_02.position = node.position
                    boss_02.name = "boss_02"
                    self.addChild(boss_02)
                    node.removeFromParent()
                    
                    boss_02.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: boss_02.size.width - 20, height: boss_02.size.height), center: boss_02.anchorPoint)
                    boss_02.physicsBody?.categoryBitMask = ColliderType.monsterCollider
                    boss_02.physicsBody?.collisionBitMask = ColliderType.tileCollider | ColliderType.wallCollider | ColliderType.bombCollider
                    boss_02.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.fireCollider
                    boss_02.physicsBody?.isDynamic = true
                    boss_02.physicsBody?.allowsRotation = false
                }
                if node.name == "boss_03" {
                    boss_03 = Boss(direction: "left")
                    boss_03.position = node.position
                    boss_03.name = "boss_03"
                    self.addChild(boss_03)
                    node.removeFromParent()
                    
                    boss_03.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: boss_03.size.width - 20, height: boss_03.size.height), center: boss_03.anchorPoint)
                    boss_03.physicsBody?.categoryBitMask = ColliderType.monsterCollider
                    boss_03.physicsBody?.collisionBitMask = ColliderType.tileCollider | ColliderType.wallCollider | ColliderType.bombCollider
                    boss_03.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.fireCollider
                    boss_03.physicsBody?.isDynamic = true
                    boss_03.physicsBody?.allowsRotation = false
                }
            }
        }
        
    }
    
    
    func setup() {// add all children to our game scene
        peerID = MCPeerID(displayName: UIDevice.current.name)
        mcSession = MCSession(peer: peerID)
        mcSession.delegate = self
        
        
        addChild(cameraNode)
        self.camera = cameraNode
        
        // setup background music
        
        // Can this work?
        bgm = SKAction.playSoundFileNamed("SpongeBob_background.mp3", waitForCompletion: true)
        
        hurt_sound = SKAction.playSoundFileNamed("hurt_sound.mp3", waitForCompletion: true)
        explosion_sound = SKAction.playSoundFileNamed("explosion_sound.mp3", waitForCompletion: true)
        item_get_sound = SKAction.playSoundFileNamed("item_get_sound.mp3", waitForCompletion: true)
        
        self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        self.physicsWorld.contactDelegate = self

        enemy.zPosition = 5
        enemy.position = CGPoint(x: -150, y: 0)
        enemy.isHidden = true
        enemy.name = "enemy"
        enemy.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: enemy.size.width - 15, height: enemy.size.height - 5), center: enemy.anchorPoint)
        enemy.physicsBody?.categoryBitMask = ColliderType.playerCollider
        enemy.physicsBody?.collisionBitMask = ColliderType.tileCollider | ColliderType.wallCollider
        enemy.physicsBody?.contactTestBitMask = ColliderType.monsterCollider | ColliderType.fireCollider | ColliderType.itemCollider
        
        enemy.physicsBody?.allowsRotation = false
        enemy.zRotation = 0
        addChild(enemy)

        
        // handle the joystick and character movement
        myCharacter.zPosition = 5
        myCharacter.position = CGPoint(x: -300, y: 0)
        myCharacter.name = "myCharacter"
        myCharacter.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: myCharacter.size.width - 15, height: myCharacter.size.height - 5), center: myCharacter.anchorPoint)
        myCharacter.physicsBody?.categoryBitMask = ColliderType.playerCollider
        myCharacter.physicsBody?.collisionBitMask = ColliderType.tileCollider | ColliderType.wallCollider
        myCharacter.physicsBody?.contactTestBitMask = ColliderType.monsterCollider | ColliderType.fireCollider | ColliderType.itemCollider
        
        
        myCharacter.physicsBody?.allowsRotation = false
        myCharacter.zRotation = 0
        addChild(myCharacter)
        
        
        
        print(self.myCharacter.health)
        self.myHealth = SKLabelNode(fontNamed: "Baskerville-Bold")
        self.myHealth.text = String(self.myCharacter.health)
        self.myHealth.fontColor = UIColor(red: 0/255, green: 255/255, blue: 255/255, alpha: 1)
        self.myHealth.zPosition = 10
        
        self.enemyHealth = SKLabelNode(fontNamed: "Baskerville-Bold")
        self.enemyHealth.text = String(self.enemy.health)
        self.enemyHealth.fontColor = UIColor(red: 0/255, green: 255/255, blue: 255/255, alpha: 1)
        self.enemyHealth.zPosition = 10
        self.enemyHealth.isHidden = true// hide until others send its characterName to me
        addChild(myHealth)
        addChild(enemyHealth)
        
        bombClicker = {
            let button = SpriteKitButton(defaultButtonImage: "red_button.png", action: {
                if self.myCharacter.bombNumber > 0 {// myCharacter can create a bomb
                    
                    let bomb = Bomb(bPos: self.myCharacter.position)
                    bomb.position = self.myCharacter.position
                    bomb.zPosition = 4
                    bomb.name = "bomb_\(self.myCharacter.characterName)_\(self.myCharacter.bombNumber)"
                    bomb.physicsBody = SKPhysicsBody(texture: bomb.bTexture, size: bomb.bTexture.size())
                    bomb.physicsBody?.categoryBitMask = ColliderType.bombCollider
                    bomb.physicsBody?.collisionBitMask = 0
                    bomb.physicsBody?.contactTestBitMask = ColliderType.fireCollider
                    self.addChild(bomb)
                    bomb.run(bomb.bombAction)
                    
                    self.myCharacter.bombNumber -= 1// only need to know my bombNumber, no need to tell others to handle my bombNumber
                    
                    // send bomb data
                    var dict = [String: String]()
                    dict["cName"] = self.myCharacter.characterName
                    dict["bomb"] = bomb.name
                    let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                    try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                    
                    var timer = Timer()
                    timer = Timer.scheduledTimer(timeInterval: 4.95, target: self, selector: #selector(self.createFires(_:)), userInfo: bomb.name, repeats: false)
                    // we create fire at 4.95 secs, so that the created fire can make contact to the original bomb
                }
                
            })
            button.zPosition = 10
            return button
        }()
        bombClicker.position = CGPoint(x: myCharacter.position.x + 300, y: myCharacter.position.y - 400)
        bombClicker.zPosition = 10
        addChild(bombClicker)
        
        
        addChild(analogJoystick)
        analogJoystick.trackingHandler = { [unowned self] data in
            
            
            //only allow the character to movehorizontally or vertically
            //if abs(data.velocity.x) > abs(data.velocity.y) {// character moves on X
            if data.velocity.x > 0 {// moves toward +X
                self.myCharacter.direction = "right"
                if(self.myCharacter.action(forKey: "RAction") == nil) {
                    self.myCharacter.run(self.myCharacter.characterRAction, withKey: "RAction")
                }
            }
            else {// moves toward -X
                self.myCharacter.direction = "left"
                if(self.myCharacter.action(forKey: "LAction") == nil) {
                    self.myCharacter.run(self.myCharacter.characterLAction, withKey: "LAction")
                }
            }
            self.myCharacter.position = CGPoint(x: self.myCharacter.position.x + data.velocity.x * self.myCharacter.velocity, y: self.myCharacter.position.y + data.velocity.y * self.myCharacter.velocity)
            
            //send x data
            var dict = [String: String]()
            dict["cName"] = self.myCharacter.characterName
            dict["moveX"] = String("\(data.velocity.x)")
            dict["moveY"] = String("\(data.velocity.y)")
            
            let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
            try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                
            //}
//            else {// character moves on Y
//                if(self.myCharacter.direction == "left") {
//                    if(self.myCharacter.action(forKey: "LAction") == nil) {
//                        self.myCharacter.run(self.myCharacter.characterLAction, withKey: "LAction")
//                    }
//                }
//                else { // myCharacter.direction == "right"
//                    if(self.myCharacter.action(forKey: "RAction") == nil) {
//                        self.myCharacter.run(self.myCharacter.characterRAction, withKey: "RAction")
//                    }
//                }
//
//                self.myCharacter.position = CGPoint(x: self.myCharacter.position.x, y: self.myCharacter.position.y + data.velocity.y * self.myCharacter.velocity)
//
//                //send y data
//                var dict = [String: String]()
//                dict["cName"] = self.myCharacter.characterName
//                dict["moveY"] = String("\(data.velocity.y)")
//
//                let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
//                try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
//
//            }
        }
        
    }
    
    
    func showConnectionPrompt() {
        let ac = UIAlertController(title: "Connect to others", message: nil, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Host a session", style: .default, handler: startHosting))
        ac.addAction(UIAlertAction(title: "Join a session", style: .default, handler: joinSession))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first

        let currentViewController: UIViewController = (keyWindow?.rootViewController!)!
        
//        let currentViewController: UIViewController = UIApplication.shared.windows.filter({$0.isKeyWindow}).first
        
        currentViewController.present(ac, animated: true, completion: nil)
    }
    
    func startHosting(action: UIAlertAction) {
    //func startHosting() {
        print("In startHosting function")
        nearbyServiceAdvertiser = MCNearbyServiceAdvertiser(peer: peerID, discoveryInfo: nil, serviceType: "Bob-bomb")
        nearbyServiceAdvertiser.delegate = self
        nearbyServiceAdvertiser.startAdvertisingPeer()
    }
    
    func joinSession(action: UIAlertAction) {
        print("In joinSession function")
        self.run(SKAction.repeatForever(bgm))
        let browser = MCBrowserViewController(serviceType: "Bob-bomb", session: mcSession)
        browser.delegate = self
        
        
        let keyWindow = UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first

        let currentViewController: UIViewController = (keyWindow?.rootViewController!)!
        
        currentViewController.present(browser, animated: true, completion: nil)
        
        
        // exchange myCharacter and enemy position
        self.enemy.position = CGPoint(x: -300, y: 0)
        self.myCharacter.position = CGPoint(x: -150, y: 0)
        
        analogJoystick.position = CGPoint(x: myCharacter.position.x - 300, y: myCharacter.position.y - 400)
        
        
        // set boss movement
        var timer = Timer()
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.setAllBossTimer), userInfo: nil, repeats: true)
    }
    
    
    public func handleTileMapNode() {
        
        for node in self.children {
            if ((node.name?.hasPrefix("tile_map")) == true) {
                print("Has prefix \"tile_map\"")
                if (node.name?.hasSuffix("01"))! {// it's tile_map_01
                    print("Find tile_map_01")
                    self.mapNode = (self.childNode(withName: node.name!) as! SKTileMapNode)
                    if let someTileMap_01: SKTileMapNode = node as? SKTileMapNode {

                        giveTileMapPhysicsBody(map: someTileMap_01, stability: 1)
                    }
                    self.mapNode.removeFromParent() // remove the original mapNode which doesn't have physicsBody
                }
                else if (node.name?.hasSuffix("02"))! {// it's tile_map_02
                    print("Find tile_map_02")
                    self.mapNode = (self.childNode(withName: node.name!) as! SKTileMapNode)
                    if let someTileMap_02: SKTileMapNode = node as? SKTileMapNode {

                        giveTileMapPhysicsBody(map: someTileMap_02, stability: 4)
                    }
                    self.mapNode.removeFromParent() // remove the original mapNode which doesn't have physicsBody
                }
            }
        }
        
    }
    
    
    func giveTileMapPhysicsBody(map: SKTileMapNode, stability: Int) {
        
        let startingLocation: CGPoint = map.position
        print("map.position: \(map.position)")

        let tileSize = map.tileSize
        let halfWidth = CGFloat(map.numberOfColumns) / 2.0 * tileSize.width
        let halfHeight = CGFloat(map.numberOfRows) / 2.0 * tileSize.height

        for col in 0..<map.numberOfColumns {
            for row in 0..<map.numberOfRows {

                if let tileDefinition = map.tileDefinition(atColumn: col, row: row) {
                    //print("tileDefinition: \(tileDefinition.textures[0])")
                    let tileArray = tileDefinition.textures
                    let tileTexture = tileArray[0]
                    let x = CGFloat(col) * tileSize.width - halfWidth + (tileSize.width / 2)
                    let y = CGFloat(row) * tileSize.height - halfHeight + (tileSize.height / 2)

                    //let tileNode = SKSpriteNode(texture: tileTexture)
                    let tileNode = TileNode(texture: tileTexture, stability: stability)
                    tileNode.position = CGPoint(x: x, y: y)
                    tileNode.physicsBody = SKPhysicsBody(texture: tileTexture, size: CGSize(width: (tileTexture.size().width), height: (tileTexture.size().height)))
                    tileNode.physicsBody?.linearDamping = 60.0// used ti simulate fluid or air friction (between 0.0~1.0)
                    tileNode.physicsBody?.affectedByGravity = false
                    tileNode.physicsBody?.categoryBitMask = ColliderType.tileCollider
                    tileNode.physicsBody?.collisionBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.fireCollider
                    //tileNode.physicsBody?.contactTestBitMask =
                    tileNode.physicsBody?.allowsRotation = false
                    tileNode.physicsBody?.isDynamic = false
                    tileNode.physicsBody?.friction = 1
                    tileNode.name = "tile_node_\(row)_\(col)"
                    self.addChild(tileNode)

                    tileNode.position = CGPoint(x: tileNode.position.x + startingLocation.x , y: tileNode.position.y + startingLocation.y)

                }
            }
        }
    }
    
    
    
    func didBegin(_ contact: SKPhysicsContact) {
//        print("Contact occurs")
//        print("A: \(String(describing: contact.bodyA.node?.name)), B: \(String(describing: contact.bodyB.node?.name))")
        
        if (contact.bodyA.node?.name == "myCharacter" && contact.bodyB.node?.name == "fire") || (contact.bodyA.node?.name == "fire" && contact.bodyB.node?.name == "myCharacter") {// myCharacter hit by fire, myCharacter.health -= 1
            self.myCharacter.run(hurt_sound)
            if(self.myCharacter.unbeatable == false) {
                print("myCharacter hit by fire")
                self.myCharacter.health -= 1
                self.myCharacter.unbeatable = true
                
                // tell others to minus myCharacter.health
                // tell others to set myCharacter.unbeatable
                var dict = [String: String]()
                dict["cName"] = self.myCharacter.characterName
                dict["setUnb"] = "true"
                dict["setHealth"] = "\(self.myCharacter.health)"
                
                let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                
                // set myCharacter.unbeatable to false
                var timer = Timer()
                timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.resetUnbeatable), userInfo: nil, repeats: false)
            }
        }
        
        if(((contact.bodyA.node?.name?.hasPrefix("tile_node")) == true) && (contact.bodyB.node?.name == "fire")) || (((contact.bodyB.node?.name?.hasPrefix("tile_node")) == true) && (contact.bodyA.node?.name == "fire")) {
            // because fire are created on both sides, we don't need to tell others which tile node should be removed, or it may delete a non-existing node
            
            print("Contact in tile_node and fire")
            if(contact.bodyA.node?.name == "fire") {// B is tile node
                if let child = self.childNode(withName: (contact.bodyB.node?.name)!) as? TileNode {
                    if child.stability > 1 {
                        child.stability -= 1
                    }
                    else {
                        child.removeFromParent()
                    }
                }
            }
            else {// A is tile node
                if let child = self.childNode(withName: (contact.bodyA.node?.name)!) as? TileNode {

                    if child.stability > 1 {
                        child.stability -= 1
                    }
                    else {
                        child.removeFromParent()
                    }
                }
            }
        }
        
        if (contact.bodyA.node?.name == "fire" && (contact.bodyB.node?.name?.hasPrefix("bomb") == true)) || (contact.bodyB.node?.name == "fire" && (contact.bodyA.node?.name?.hasPrefix("bomb") == true)) {
            // remove bomb and create new fire on it position
            print("Fire and Bomb Contact")
            self.run(explosion_sound)
            if(contact.bodyA.node?.name == "fire") {// A is fire, B is bomb
                if let child = self.childNode(withName: (contact.bodyB.node?.name)!) {

                    // here change ! to == true will cause exception, why?
                    if (child.name?.contains(self.myCharacter.characterName))! {
                        let fire_x = Fire(firePower: self.myCharacter.firePower, direction: "x")
                        fire_x.position = child.position
                        fire_x.zPosition = 4
                        fire_x.name = "fire"
                        fire_x.physicsBody = SKPhysicsBody(texture: fire_x.texture!, size: (fire_x.texture?.size())!)
                        fire_x.physicsBody?.categoryBitMask = ColliderType.fireCollider
                        fire_x.physicsBody?.collisionBitMask = 0
                        fire_x.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
                        
                        let fire_y = Fire(firePower: self.myCharacter.firePower, direction: "y")
                        fire_y.position = child.position
                        fire_y.zPosition = 4
                        fire_y.name = "fire"
                        fire_y.physicsBody = SKPhysicsBody(texture: fire_y.texture!, size: (fire_y.texture?.size())!)
                        fire_y.physicsBody?.categoryBitMask = ColliderType.fireCollider
                        fire_y.physicsBody?.collisionBitMask = 0
                        fire_y.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
                        
                        var dict = [String: String]()
                        dict["cName"] = self.myCharacter.characterName
                        dict["removeChild"] = child.name!
                        
                        child.removeFromParent()
                        self.myCharacter.bombNumber += 1// add 1 to that bomb's owner's bombNumber
                        
                        
                        self.addChild(fire_x)
                        fire_x.run(fire_x.fireAction)
                        self.addChild(fire_y)
                        fire_y.run(fire_y.fireAction)
                        
                        
                        dict["fire"] = String("\(fire_x.position.x),\(fire_x.position.y),\(self.myCharacter.firePower)")
                        
                        //print("send from bomb and fire contact")
                        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                    }
//                    else {
//                        let fire_x = Fire(firePower: self.enemy.firePower, direction: "x")
//                        fire_x.position = child.position
//                        fire_x.zPosition = 4
//                        fire_x.name = "fire"
//                        fire_x.physicsBody = SKPhysicsBody(texture: fire_x.texture!, size: (fire_x.texture?.size())!)
//                        fire_x.physicsBody?.categoryBitMask = ColliderType.fireCollider
//                        fire_x.physicsBody?.collisionBitMask = 0
//                        fire_x.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
//                        self.addChild(fire_x)
//                        fire_x.run(fire_x.fireAction)
//
//                        let fire_y = Fire(firePower: self.enemy.firePower, direction: "y")
//                        fire_y.position = child.position
//                        fire_y.zPosition = 4
//                        fire_y.name = "fire"
//                        fire_y.physicsBody = SKPhysicsBody(texture: fire_y.texture!, size: (fire_y.texture?.size())!)
//                        fire_y.physicsBody?.categoryBitMask = ColliderType.fireCollider
//                        fire_y.physicsBody?.collisionBitMask = 0
//                        fire_y.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
//                        self.addChild(fire_y)
//                        fire_y.run(fire_y.fireAction)
//
//                        var dict = [String: String]()
//                        dict["cName"] = self.myCharacter.characterName
//                        dict["fire"] = String("\(fire_x.position.x),\(fire_x.position.y),\(self.enemy.firePower)")
//
//                        print("send from bomb and fire contact")
//                        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
//                        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
//                    }

                }
            }
            else {// A is bomb, B is fire
                if let child = self.childNode(withName: (contact.bodyA.node?.name)!) {

                    // here change ! to == true will cause exception, why?
                    if (child.name?.contains(self.myCharacter.characterName))! {// use myCharacter's firePower
                        
                        let fire_x = Fire(firePower: self.myCharacter.firePower, direction: "x")
                        fire_x.position = child.position
                        fire_x.zPosition = 4
                        fire_x.name = "fire"
                        fire_x.physicsBody = SKPhysicsBody(texture: fire_x.texture!, size: (fire_x.texture?.size())!)
                        fire_x.physicsBody?.categoryBitMask = ColliderType.fireCollider
                        fire_x.physicsBody?.collisionBitMask = 0
                        fire_x.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
                        
                        let fire_y = Fire(firePower: self.myCharacter.firePower, direction: "y")
                        fire_y.position = child.position
                        fire_y.zPosition = 4
                        fire_y.name = "fire"
                        fire_y.physicsBody = SKPhysicsBody(texture: fire_y.texture!, size: (fire_y.texture?.size())!)
                        fire_y.physicsBody?.categoryBitMask = ColliderType.fireCollider
                        fire_y.physicsBody?.collisionBitMask = 0
                        fire_y.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
                        
                        var dict = [String: String]()
                        dict["cName"] = self.myCharacter.characterName
                        dict["removeChild"] = child.name!
                        
                        child.removeFromParent()
                        self.myCharacter.bombNumber += 1
                        
                        self.addChild(fire_x)
                        fire_x.run(fire_x.fireAction)
                        self.addChild(fire_y)
                        fire_y.run(fire_y.fireAction)
                        
                        dict["fire"] = String("\(fire_x.position.x),\(fire_x.position.y),\(self.myCharacter.firePower)")
                        print("send from bomb and fire contact")
                        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                        
                    }
//                    else {// use enemy's firePower
//                        let fire_x = Fire(firePower: self.enemy.firePower, direction: "x")
//                        fire_x.position = child.position
//                        fire_x.zPosition = 4
//                        fire_x.name = "fire"
//                        fire_x.physicsBody = SKPhysicsBody(texture: fire_x.texture!, size: (fire_x.texture?.size())!)
//                        fire_x.physicsBody?.categoryBitMask = ColliderType.fireCollider
//                        fire_x.physicsBody?.collisionBitMask = 0
//                        fire_x.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
//                        
//                        self.addChild(fire_x)
//                        fire_x.run(fire_x.fireAction)
//                        
//                        let fire_y = Fire(firePower: self.enemy.firePower, direction: "y")
//                        fire_y.position = child.position
//                        fire_y.zPosition = 4
//                        fire_y.name = "fire"
//                        fire_y.physicsBody = SKPhysicsBody(texture: fire_y.texture!, size: (fire_y.texture?.size())!)
//                        fire_y.physicsBody?.categoryBitMask = ColliderType.fireCollider
//                        fire_y.physicsBody?.collisionBitMask = 0
//                        fire_y.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
//                        
//                        self.addChild(fire_y)
//                        fire_y.run(fire_y.fireAction)
//                        
//                        var dict = [String: String]()
//                        dict["cName"] = self.myCharacter.characterName
//                        dict["fire"] = String("\(fire_x.position.x),\(fire_x.position.y),\(self.myCharacter.firePower)")
//                        print("send from bomb and fire contact")
//                        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
//                        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
//                    }
                }
            }
        }
        
        
        
        if (contact.bodyA.node?.name == "myCharacter" && ((contact.bodyB.node?.name!.contains("item")) == true)) || (contact.bodyB.node?.name == "myCharacter" && ((contact.bodyA.node?.name!.contains("item")) == true)) {
            print("Touch an item!")
            
            if contact.bodyA.node?.name == "myCharacter" {// check which item B is
                if let child = self.childNode(withName: (contact.bodyB.node?.name)!) {
                    if (child.name!.contains("water") == true) {
                        if self.myCharacter.characterName == "bob" {
                            self.myCharacter.velocity += 0.01
                        }
                        else {
                            self.myCharacter.velocity -= 0.01
                        }
                        
                        var dict = [String: String]()
                        dict["cName"] = self.myCharacter.characterName
                        dict["setVelocity"] = "\(self.myCharacter.velocity)"
                        
                        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                    }
                    else if (child.name!.contains("bomb") == true) {
                        self.myCharacter.bombNumber += 1
                    }
                    else if (child.name!.contains("burger") == true) {
                        self.myCharacter.health += 1
                        
                        var dict = [String: String]()
                        dict["cName"] = self.myCharacter.characterName
                        dict["setHealth"] = "\(self.myCharacter.health)"
                        
                        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                    }
                    else if (child.name!.contains("potion") == true) {
                        print("Get potion")
                        if (self.myCharacter.firePower < 3) {
                            self.myCharacter.firePower += 1
                            
                            var dict = [String: String]()
                            dict["cName"] = self.myCharacter.characterName
                            dict["setFirePower"] = "\(self.myCharacter.firePower)"
                            
                            let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                            try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                        }
                        
                    }
                    else if (child.name!.contains("shoe") == true) {
                        self.myCharacter.velocity *= 2
                        
                        var dict = [String: String]()
                        dict["cName"] = self.myCharacter.characterName
                        dict["setVelocity"] = "\(self.myCharacter.velocity)"
                        
                        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                    }
                    else {
                        print("Unknown item")
                    }
                    self.myCharacter.run(item_get_sound)
                    child.removeFromParent()
                    
                    var dict = [String: String]()
                    dict["cName"] = self.myCharacter.characterName
                    dict["removeChild"] = child.name!
                    
                    let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                    try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                }
            }
            else {// check which item A is
                if let child = self.childNode(withName: (contact.bodyA.node?.name)!) {
                    if (child.name!.contains("water") == true) {
                        if self.myCharacter.characterName == "bob" {
                            self.myCharacter.velocity += 0.01
                        }
                        else {
                            self.myCharacter.velocity -= 0.01
                        }
                        
                        var dict = [String: String]()
                        dict["cName"] = self.myCharacter.characterName
                        dict["setVelocity"] = "\(self.myCharacter.velocity)"
                        
                        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                    }
                    else if (child.name!.contains("bomb") == true) {
                        self.myCharacter.bombNumber += 1
                    }
                    else if (child.name!.contains("burger") == true) {
                        self.myCharacter.health += 1
                        
                        var dict = [String: String]()
                        dict["cName"] = self.myCharacter.characterName
                        dict["setHealth"] = "\(self.myCharacter.health)"
                        
                        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                    }
                    else if (child.name!.contains("potion") == true) {
                        print("Get potion")
                        
                        if (self.myCharacter.firePower < 3) {
                            self.myCharacter.firePower += 1
                            
                            var dict = [String: String]()
                            dict["cName"] = self.myCharacter.characterName
                            dict["setFirePower"] = "\(self.myCharacter.firePower)"
                            
                            let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                            try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                        }
                    }
                    else if (child.name!.contains("shoe") == true) {
                        self.myCharacter.velocity *= 2
                        
                        var dict = [String: String]()
                        dict["cName"] = self.myCharacter.characterName
                        dict["setVelocity"] = "\(self.myCharacter.velocity)"
                        
                        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                    }
                    else {
                        print("Unknown item")
                    }
                    self.myCharacter.run(item_get_sound)
                    child.removeFromParent()
                    
                    var dict = [String: String]()
                    dict["cName"] = self.myCharacter.characterName
                    dict["removeChild"] = child.name!
                    
                    let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
                    try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
                }
            }
        }
        
        if ((contact.bodyA.node?.name?.contains("boss")) == true && contact.bodyB.node?.name == "myCharacter") || ((contact.bodyB.node?.name?.contains("boss")) == true && contact.bodyA.node?.name == "myCharacter") {
            
            self.myCharacter.health -= 1
            self.myCharacter.unbeatable = true
            var dict = [String: String]()
            dict["cName"] = self.myCharacter.characterName
            dict["setUnb"] = "true"
            dict["setHealth"] = "\(self.myCharacter.health)"
            let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
            try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
            
            var timer = Timer()
            timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.resetUnbeatable), userInfo: nil, repeats: false)
        }
        
        
    }
    
    
    
    @objc func createFires(_ timer: Timer) {
        //self.myCharacter.bombNumber += 1
//        let bomb = timer.userInfo as! Bomb
        //print("timer.userInfo: \(timer.userInfo)")
        if let bomb = self.childNode(withName: timer.userInfo as! String) as? Bomb {// if that bomb still exists, remove it
            print("Find timeup bomb withname: \(String(describing: bomb.name)), create fire and remove it")
            
            // check the bomb belongs to whom, get that man's firePower
            let fire_x = Fire(firePower: self.myCharacter.firePower, direction: "x")
            fire_x.position = bomb.position
            fire_x.zPosition = 4
            fire_x.name = "fire"
            fire_x.physicsBody = SKPhysicsBody(texture: fire_x.texture!, size: (fire_x.texture?.size())!)
            fire_x.physicsBody?.categoryBitMask = ColliderType.fireCollider
            fire_x.physicsBody?.collisionBitMask = 0
            fire_x.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
            addChild(fire_x)
            fire_x.run(fire_x.fireAction)
            
            let fire_y = Fire(firePower: self.myCharacter.firePower, direction: "y")
            fire_y.position = bomb.position
            fire_y.zPosition = 4
            fire_y.name = "fire"
            fire_y.physicsBody = SKPhysicsBody(texture: fire_y.texture!, size: (fire_y.texture?.size())!)
            fire_y.physicsBody?.categoryBitMask = ColliderType.fireCollider
            fire_y.physicsBody?.collisionBitMask = 0
            fire_y.physicsBody?.contactTestBitMask = ColliderType.playerCollider | ColliderType.monsterCollider | ColliderType.tileCollider | ColliderType.bombCollider
            addChild(fire_y)
            fire_y.run(fire_y.fireAction)
            
            // send  firex, firey spawn position
            var dict = [String: String]()
            dict["cName"] = self.myCharacter.characterName
            dict["fire"] = String("\(fire_x.position.x),\(fire_x.position.y),\(self.myCharacter.firePower)")
            print("send from createFires function")
            let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
            try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
        }
        
    }
    
    @objc func resetUnbeatable() {// reset myCharacter.unbeatable to false
        print("reset myCharacter.unbeatable to false")
        self.myCharacter.unbeatable = false
        print("\(self.myCharacter.unbeatable)")
        
        // send mesage to tell others
        var dict = [String: String]()
        dict["cName"] = self.myCharacter.characterName
        dict["setUnb"] = "false"
        
        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)

    }
    
    
    
    @objc func setAllBossTimer() {
        var timer_01 = Timer()
        timer_01 = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.boss1Move), userInfo: nil, repeats: true)
        var timer_02 = Timer()
        timer_02 = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.boss2Move), userInfo: nil, repeats: true)
        var timer_03 = Timer()
        timer_03 = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.boss3Move), userInfo: nil, repeats: true)
    }
    
    @objc func boss1Move() {
        // only called by the joining one
        // tell others to update bosses's position
        let direction_x = Int.random(in: 0...1)
        let direction_y = Int.random(in: 0...1)
        let magnitude_x = CGFloat(Int.random(in: 30...50))
        let magnitude_y = CGFloat(Int.random(in: 30...50))
        
        if direction_x == 0 { // +x
            
            if (boss_01.action(forKey: "RAction") == nil) {
                boss_01.removeAllActions()
                boss_01.run(SKAction.repeatForever(boss_01.bossRAction), withKey: "RAction")
            }
            
            if direction_y == 0 {// +y
                let vec = CGVector(dx: magnitude_x, dy: magnitude_y)
                self.boss_01.physicsBody?.applyForce(vec)
            }
            else {// -y
                let vec = CGVector(dx: magnitude_x, dy: -magnitude_y)
                self.boss_01.physicsBody?.applyForce(vec)
            }
            
            
        }
        else { // -x
            
            if (boss_01.action(forKey: "LAction") == nil) {
                boss_01.removeAllActions()
                boss_01.run(SKAction.repeatForever(boss_01.bossLAction), withKey: "LAction")
            }
            
            if direction_y == 0 {// +y
                let vec = CGVector(dx: -magnitude_x, dy: magnitude_y)
                self.boss_01.physicsBody?.applyForce(vec)
            }
            else {// -y
                let vec = CGVector(dx: -magnitude_x, dy: -magnitude_y)
                self.boss_01.physicsBody?.applyForce(vec)
            }
        }
        
        
        
        // tell others that the enemy is applied a force
        var dict = [String: String]()
        dict["cName"] = self.myCharacter.characterName
        dict["applyForce"] = "boss_01,\(direction_x),\(direction_y),\(magnitude_x),\(magnitude_y)"
        
        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
    }
    
    @objc func boss2Move() {
        // only called by the joining one
        // tell others to update bosses's position
        let direction_x = Int.random(in: 0...1)
        let direction_y = Int.random(in: 0...1)
        let magnitude_x = CGFloat(Int.random(in: 30...50))
        let magnitude_y = CGFloat(Int.random(in: 30...50))
        
        if direction_x == 0 { // +x
            
            if (boss_02.action(forKey: "RAction") == nil) {
                boss_02.removeAllActions()
                boss_02.run(SKAction.repeatForever(boss_02.bossRAction), withKey: "RAction")
            }
            
            if direction_y == 0 {// +y
                let vec = CGVector(dx: magnitude_x, dy: magnitude_y)
                self.boss_02.physicsBody?.applyForce(vec)
            }
            else {// -y
                let vec = CGVector(dx: magnitude_x, dy: -magnitude_y)
                self.boss_02.physicsBody?.applyForce(vec)
            }
            
            
        }
        else { // -x
            
            if (boss_02.action(forKey: "LAction") == nil) {
                boss_02.removeAllActions()
                boss_02.run(SKAction.repeatForever(boss_02.bossLAction), withKey: "LAction")
            }
            
            if direction_y == 0 {// +y
                let vec = CGVector(dx: -magnitude_x, dy: magnitude_y)
                self.boss_02.physicsBody?.applyForce(vec)
            }
            else {// -y
                let vec = CGVector(dx: -magnitude_x, dy: -magnitude_y)
                self.boss_02.physicsBody?.applyForce(vec)
            }
        }
        
        
        
        // tell others that the enemy is applied a force
        var dict = [String: String]()
        dict["cName"] = self.myCharacter.characterName
        dict["applyForce"] = "boss_02,\(direction_x),\(direction_y),\(magnitude_x),\(magnitude_y)"
        
        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
    }
    
    @objc func boss3Move() {
        // only called by the joining one
        // tell others to update bosses's position
        let direction_x = Int.random(in: 0...1)
        let direction_y = Int.random(in: 0...1)
        let magnitude_x = CGFloat(Int.random(in: 30...50))
        let magnitude_y = CGFloat(Int.random(in: 30...50))
        
        if direction_x == 0 { // +x
            
            if (boss_03.action(forKey: "RAction") == nil) {
                boss_03.removeAllActions()
                boss_03.run(SKAction.repeatForever(boss_03.bossRAction), withKey: "RAction")
            }
            
            if direction_y == 0 {// +y
                let vec = CGVector(dx: magnitude_x, dy: magnitude_y)
                self.boss_03.physicsBody?.applyForce(vec)
            }
            else {// -y
                let vec = CGVector(dx: magnitude_x, dy: -magnitude_y)
                self.boss_03.physicsBody?.applyForce(vec)
            }
            
            
        }
        else { // -x
            
            if (boss_03.action(forKey: "LAction") == nil) {
                boss_03.removeAllActions()
                boss_03.run(SKAction.repeatForever(boss_03.bossLAction), withKey: "LAction")
            }
            
            if direction_y == 0 {// +y
                let vec = CGVector(dx: -magnitude_x, dy: magnitude_y)
                self.boss_03.physicsBody?.applyForce(vec)
            }
            else {// -y
                let vec = CGVector(dx: -magnitude_x, dy: -magnitude_y)
                self.boss_03.physicsBody?.applyForce(vec)
            }
        }
        
        
        
        // tell others that the enemy is applied a force
        var dict = [String: String]()
        dict["cName"] = self.myCharacter.characterName
        dict["applyForce"] = "boss_03,\(direction_x),\(direction_y),\(magnitude_x),\(magnitude_y)"
        
        let data = try? JSONSerialization.data(withJSONObject: dict, options: [])
        try? self.mcSession.send(data!, toPeers: self.mcSession.connectedPeers, with: .reliable)
    }
}
