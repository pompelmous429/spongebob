//
//  Fire.swift
//  SpongeBob
//
//  Created by mac19 on 2021/6/11.
//  Copyright © 2021 mac19. All rights reserved.
//

import Foundation
import SpriteKit

class Fire: SKSpriteNode {
    
    var fTexture: SKTexture!
    var fireAction: SKAction!

    
    init(firePower: Int, direction: String) {// firePower should be [1...3]
        
        if direction == "x" {
            fTexture = SKTexture(imageNamed: "fire_x_0\(firePower).png")
        }
        else if (direction == "y") {
            fTexture = SKTexture(imageNamed: "fire_y_0\(firePower).png")
        }
        else {
            print("Unknown direction in Fire()")
        }
        
        fireAction = SKAction.sequence([SKAction.wait(forDuration: 0.5), SKAction.removeFromParent()])
        
        
        super.init(texture: fTexture, color: UIColor.clear, size: fTexture.size())
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
