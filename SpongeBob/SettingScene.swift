//
//  SettingScene.swift
//  SpongeBob
//
//  Created by mac19 on 2021/6/10.
//  Copyright © 2021 mac19. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class SettingScene: SKScene {
    var characterButton: DropDownButton = DropDownButton()
    var mapButton: DropDownButton = DropDownButton()
    
    
    
    override func didMove(to view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "setting_background_02.png")
        background.size = CGSize(width: self.frame.width, height: self.frame.height)
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        addChild(background)
        
//        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
//        backgroundImage.image = UIImage(named: "patrick_background.jpg")
//        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
//        self.view?.insertSubview(backgroundImage, at: 0)
                
                
//
//        let firstGuide = UILayoutGuide()
//        self.view?.addLayoutGuide(firstGuide)
//        firstGuide.heightAnchor.constraint(equalTo: self.view!.heightAnchor, multiplier: 1/3).isActive = true
//        firstGuide.topAnchor.constraint(equalTo: self.view!.topAnchor).isActive = true

                
        characterButton = DropDownButton(frame: CGRect(x: self.frame.width / 2 - 100, y: self.frame.height / 2 - 50 - 250, width: 200, height: 100))
        characterButton.setTitle("Character", for: .normal)
        //"角色"
        characterButton.titleLabel?.font = UIFont(name: "Baskerville-Bold", size: 36)
        characterButton.setTitleColor(UIColor.yellow, for: .normal)
        //characterButton.translatesAutoresizingMaskIntoConstraints = false
        self.view?.addSubview(characterButton)
                
        //characterButton.centerYAnchor.constraint(equalTo: firstGuide.bottomAnchor).isActive = true
        //characterButton.centerXAnchor.constraint(equalTo: self.view!.centerXAnchor).isActive = true
        //        characterButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        //
//        characterButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
//        characterButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
        characterButton.dropView.dropDownOptions = ["bob", "patrick", "squid"]
                
                
//        let secondGuide = UILayoutGuide()
//        self.view?.addLayoutGuide(secondGuide)
//        secondGuide.heightAnchor.constraint(equalTo: self.view!.heightAnchor, multiplier: 2/3).isActive = true
//        secondGuide.topAnchor.constraint(equalTo: self.view!.topAnchor).isActive = true
                
                
        mapButton = DropDownButton(frame: CGRect(x: self.frame.width / 2 - 100, y: self.frame.height / 2 - 50, width: 200, height: 100))
        mapButton.setTitle("Map", for: .normal)
        mapButton.titleLabel?.font = UIFont(name: "Baskerville-Bold", size: 36)
        //"地圖"
        //AvenirNextCondensed-Bold
        mapButton.setTitleColor(UIColor.yellow, for: .normal)
//        mapButton.translatesAutoresizingMaskIntoConstraints = false
        self.view?.addSubview(mapButton)
                        
//        mapButton.centerYAnchor.constraint(equalTo: secondGuide.bottomAnchor).isActive = true
//        mapButton.centerXAnchor.constraint(equalTo: self.view!.centerXAnchor).isActive = true
////        characterButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
//                //
//        mapButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
//        mapButton.heightAnchor.constraint(equalToConstant: 100).isActive = true
        mapButton.dropView.dropDownOptions = ["Map_01", "Map_02", "Map_03"]
        
        
        
        let enterGameButton: SpriteKitButton = {
           let button = SpriteKitButton(defaultButtonImage: "start_button_02.png", action: {
            print("Startbutton clicked")
            print("Character: \(String(describing: self.characterButton.currentTitle!)), Map: \(String(self.mapButton.currentTitle!))")
            
            
            if(String(self.mapButton.currentTitle!).hasSuffix("01")) {
                print("It's map 01")
                
                let gameScene = SKScene(fileNamed: "MyGameScene_1")
                
                // pass character data to game scene
                gameScene?.userData = NSMutableDictionary()
                gameScene?.userData?.setValue(self.characterButton.currentTitle, forKey: "cName") // pass ("cName", "bob/patrick/squid") to game scene
                
                self.view?.presentScene(gameScene)
                
                self.characterButton.isHidden = true
                self.mapButton.isHidden = true
                
                
            }
            else if (String(self.mapButton.currentTitle!).hasSuffix("02")) {
                print("It's map 02")
                
                let gameScene = SKScene(fileNamed: "MyGameScene_2")
                
                // pass character data to game scene
                gameScene?.userData = NSMutableDictionary()
                gameScene?.userData?.setValue(self.characterButton.currentTitle, forKey: "cName") // pass ("cName", "bob/patrick/squid") to game scene
                
                self.view?.presentScene(gameScene)
                
                self.characterButton.isHidden = true
                self.mapButton.isHidden = true
                
            }
            else if (String(self.mapButton.currentTitle!).hasSuffix("03")) {
                print("It's map 03")
                
                let gameScene = SKScene(fileNamed: "MyGameScene_3")
                
                // pass character data to game scene
                gameScene?.userData = NSMutableDictionary()
                gameScene?.userData?.setValue(self.characterButton.currentTitle, forKey: "cName") // pass ("cName", "bob/patrick/squid") to game scene
                
                self.view?.presentScene(gameScene)
                
                self.characterButton.isHidden = true
                self.mapButton.isHidden = true
                
            }
            
           })
            button.zPosition = 3
            return button
        }()
        
        enterGameButton.position = CGPoint(x: self.frame.size.width / 2 - 100, y: self.frame.size.height / 2 - 300)
        self.addChild(enterGameButton)
        print("enterGameButton position: \(enterGameButton.position)")
    }
}
