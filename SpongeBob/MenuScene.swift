//
//  MenuScene.swift
//  SpongeBob
//
//  Created by user198529 on 5/21/21.
//  Copyright © 2021 mac19. All rights reserved.
//

import UIKit
import SpriteKit

class MenuScene: SKScene {
    
    
//    lazy var startButton: BDButton = {
//        var button = BDButton(imageNamed: "gameStart.png", buttonAction: {
//            // button action
//            print("button width: \(button.frame.width)")
//        })
//        button.zPosition = 1
//        return button
//    }()
    
    
    
    
    
    
    // Called after the view controller is added to or removed from a container view controller
    override func didMove(to view: SKView) {
        createScene()
        
        
        
        let startButton: SpriteKitButton = {
            let button = SpriteKitButton(defaultButtonImage: "gameStart.png", action: {
                let settingScene = SettingScene(size: (self.view?.bounds.size)!)
                settingScene.scaleMode = .aspectFill
                self.view?.presentScene(settingScene)
            })
            button.zPosition = 1
            return button
        }()
        
        let gameIntro: SpriteKitButton = {
            let button = SpriteKitButton(defaultButtonImage: "gameIntro.png", action: {
                
                let keyWindow = UIApplication.shared.connectedScenes
                    .filter({$0.activationState == .foregroundActive})
                    .map({$0 as? UIWindowScene})
                    .compactMap({$0})
                    .first?.windows
                    .filter({$0.isKeyWindow}).first

                let currentViewController: UIViewController = (keyWindow?.rootViewController!)!
                
                let rootVC = BackgroundViewController()
                let navVC = UINavigationController(rootViewController: rootVC)
                //navVC.modalPresentationStyle = .fullScreen
                
                
                currentViewController.present(navVC, animated: true)
                
            })
            button.zPosition = 1
            return button
        }()
        
        let quitGame: SpriteKitButton = {
            let button = SpriteKitButton(defaultButtonImage: "quitGame.png", action: {
                
                // action to quit game
                exit(0)
            })
            button.zPosition = 1
            return button
        }()
        
        
        
        
        
        
        //anchorPoint = CGPoint(x: 0.5, y: 0.5)
        startButton.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 - 80)
        gameIntro.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 - 210)
        quitGame.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 - 340)
        
        
        addChild(startButton)
        addChild(gameIntro)
        addChild(quitGame)
        
        
        
        print(startButton.size)
        print(startButton.position)// return playButton's center
        print(startButton.calculateAccumulatedFrame())
    }
    
    
    func createScene() {
        print("createScene in MenuScene")
        let bgd = SKSpriteNode(imageNamed: "menu_background_02.png")
        bgd.size.width = self.size.width
        bgd.size.height = self.size.height
        bgd.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        bgd.zPosition = -1
        
        
        // we need three buttons
        
        
        
        self.addChild(bgd)
        
    }
}


class BackgroundViewController: UIViewController {
    
    let button = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemRed
        title = "遊戲背景"
        
        let imageView = UIImageView(image: UIImage(named: "intro_02.png"))
        
        view.addSubview(imageView)

        
        button.setTitle("遊戲操作", for: .normal)
        button.backgroundColor = .purple
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        button.frame = CGRect(x: ScreenSize.width * 0.3, y: ScreenSize.height * 0.77, width: ScreenSize.width * 0.3, height: ScreenSize.height * 0.05)
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(goIntro), for: .touchUpInside)
        
        view.addSubview(button)
        
    }

    @objc func goIntro() {
        let introVC = IntroViewController()
        navigationController?.pushViewController(introVC, animated: true)
        
    }
}



class IntroViewController: UIViewController {
    let button = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemRed
        title = "遊戲操作"
        
        let imageView = UIImageView(image: UIImage(named: "menu_intro_2.png"))

        
        view.addSubview(imageView)

        
        button.setTitle("回主選單", for: .normal)
        button.backgroundColor = .purple
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        button.frame = CGRect(x: ScreenSize.width * 0.3, y: ScreenSize.height * 0.77, width: ScreenSize.width * 0.3, height: ScreenSize.height * 0.05)
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(goMenu), for: .touchUpInside)
        
        view.addSubview(button)
        
    }

    @objc func goMenu() {
        
        dismiss(animated: true, completion: nil)
    }
}
