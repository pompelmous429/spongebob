//
//  GameOverScene.swift
//  SpongeBob
//
//  Created by mac19 on 2021/6/12.
//  Copyright © 2021 mac19. All rights reserved.
//

import Foundation
import SpriteKit

class GameOverScene: SKScene {
    
    var winnerLabel: SKLabelNode!
    var winnerString: String!
    override func didMove(to view: SKView) {
        
        let background = SKSpriteNode(imageNamed: "gameover_background_03.png")
        background.size = CGSize(width: self.frame.width, height: self.frame.height)
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        addChild(background)
        
        
        let restartButton: SpriteKitButton = {
            let button = SpriteKitButton(defaultButtonImage: "go_restart_button.png", action: {
                let menuScene = MenuScene(size: (self.view?.bounds.size)!)
                menuScene.scaleMode = .aspectFill
                self.view?.presentScene(menuScene)
            })
            button.zPosition = 1
            return button
        }()
        
        let quitGame: SpriteKitButton = {
            let button = SpriteKitButton(defaultButtonImage: "go_quit_button.png", action: {
                
                // action to quit game
                exit(0)
            })
            button.zPosition = 1
            return button
        }()
        
        restartButton.position = CGPoint(x: self.frame.width / 2 - 80, y: self.frame.height / 2 + 150)
        quitGame.position = CGPoint(x: self.frame.width / 2 + 240, y: self.frame.height / 2 + 150)
        
        addChild(restartButton)
        addChild(quitGame)
    }
    
}
